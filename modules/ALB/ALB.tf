# ----------------------------
#External Load balancer for reverse proxy nginx
#---------------------------------

resource "aws_lb" "david-alb" {
  name            = "david-alb"
  internal        = false
  security_groups = [var.public-sg]
  subnets = [var.public-sbn-1,
  var.public-sbn-2, ]

  tags = {
    Name = "david-alb"
  }

  ip_address_type    = "ipv4"
  load_balancer_type = "application"
}

#--- create a target group for the load balancer
resource "aws_lb_target_group" "david-nginx-tgt" {
  health_check {
    interval            = 10
    path                = "/healthstatus"
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
  }
  name        = "david-nginx-tgt"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = var.vpc_id
}

#--- create a listener for the load balancer

resource "aws_lb_listener" "david-nginx-listner" {
  load_balancer_arn = aws_lb.david-alb.arn
  port              = 443
  protocol          = "HTTPS"
  certificate_arn   = aws_acm_certificate_validation.oyindamola.certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.david-nginx-tgt.arn
  }
}

resource "aws_lb_listener" "david-nginx-listner-80" {
  load_balancer_arn = aws_lb.david-alb.arn
  port              = 80
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.david-nginx-tgt.arn
  }
}

# ----------------------------
#Internal Load Balancers for webservers
#---------------------------------

resource "aws_lb" "david-ialb" {
  name            = "david-ialb"
  internal        = true
  security_groups = [var.private-sg]
  subnets = [var.private-sbn-1,
  var.private-sbn-2, ]

  tags = {
    Name = "david-ialb"
  }

  ip_address_type    = "ipv4"
  load_balancer_type = "application"
}


# --- target group and listener for wordpress -------

resource "aws_lb_target_group" "david-wordpress-tgt" {
  health_check {
    interval            = 10
    path                = "/healthstatus"
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
  }

  name        = "david-wordpress-tgt"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = var.vpc_id
}


# --- target group for tooling -------

resource "aws_lb_target_group" "david-tooling-tgt" {
  health_check {
    interval            = 10
    path                = "/healthstatus"
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
  }

  name        = "david-tooling-tgt"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = var.vpc_id
}

# For this aspect a single listener was created for the wordpress which is default,
# A rule was created to route traffic to tooling when the host header changes


resource "aws_lb_listener" "david-web-listener" {
  load_balancer_arn = aws_lb.david-ialb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.david-wordpress-tgt.arn
  }
}

# listener rule for tooling target

resource "aws_lb_listener_rule" "david-listener" {
  listener_arn = aws_lb_listener.david-web-listener.arn
  priority     = 99

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.david-tooling-tgt.arn
  }

  condition {
    host_header {
      values = ["tooling.livingstone.cf"]
    }
  }
}














