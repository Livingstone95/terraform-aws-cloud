#output the External Load balancaer DNS
output "alb_dns_name" {
  description = "External Load balancaer DNS"
  value       = aws_lb.david-alb.dns_name
}

# output the External Load balancaer target group
output "david-nginx-tgt" {
  description = "External Load balancaer target group"
  value       = aws_lb_target_group.david-nginx-tgt.arn
}


# Output External Load balancaer target group
output "david-wordpress-tgt" {
  description = "External Load balancaer target group"
  value       = aws_lb_target_group.david-wordpress-tgt.arn
}



# Output the External Load balancaer target group
output "david-tooling-tgt" {
  description = "External Load balancaer target group"
  value       = aws_lb_target_group.david-tooling-tgt.arn
}



